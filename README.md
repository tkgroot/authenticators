# Authenticators (WIP)

A collection of authentication services which can be used to build a front-end against in development mode.

## Roadmap or Feature-Requirements

- [x] create initial commit with bare-bone application
- [ ] add jwt handlers
- [ ] build CLI to add/register users manually
- [ ] add tests
- [ ] add authorization possibilities
  - [ ] create database roles for users (1-to-many or many-to-many)
  - [ ] create resource for user-role x and y
- [ ] create additional register/register-confirm endpoints 
- [ ] create RESTful implementation for authenticating against
  - [ ] add /me endpoint for users
- [ ] Open API
  - [ ] API documentation
  - [ ] Swagger huh?
- [ ] API token
  - [ ] Users endpoint generating API token
- [ ] fe-ba contracts
- [ ] add GraphQL implementation to authenticate against
- [ ] implement authenticators in different languages
- [ ] OAuth 2.x implementation?
- [ ] add useful code style dependencies and pre-commit-hooks
- [ ] TLS/SSL HTTP with certificates
- [ ] HTTP2
- [ ] 2FA
