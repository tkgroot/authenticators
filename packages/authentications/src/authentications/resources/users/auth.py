from flask_restful import Resource, reqparse
from flask_api.status import HTTP_204_NO_CONTENT, HTTP_401_UNAUTHORIZED, HTTP_200_OK
from flask_jwt_extended import create_access_token
from authentications.models.users import Users

parser = reqparse.RequestParser()
parser.add_argument("username", location="json", required=True)
parser.add_argument("password", location="json", required=True)


class Authentication(Resource):
    """Authentication."""

    @staticmethod
    def get():
        return None, HTTP_204_NO_CONTENT

    @staticmethod
    def post():
        form = parser.parse_args()
        user = Users.query.filter_by(username=form.get("username")).first()

        if user is None or not user.has_valid_password(form.get("password")):
            return None, HTTP_401_UNAUTHORIZED

        user.has_logged_in()
        return {"token": create_access_token(user.uuid)}, HTTP_200_OK
