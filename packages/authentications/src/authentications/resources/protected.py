from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity
from authentications.models.users import Users, user_schema


class Protected(Resource):
    @jwt_required
    def get(self):
        user = Users.query.filter_by(uuid=get_jwt_identity()).first()

        return user_schema.dump(user)
