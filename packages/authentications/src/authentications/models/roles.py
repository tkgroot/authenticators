from authentications.extensions import db
from authentications.models.tables.roles_permissions import RolesPermissionsTbl
from authentications.utils.mixins.model import TimestampMixin


class Roles(TimestampMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    name = db.Column(db.String(64), nullable=False, index=True)
    description = db.Column(db.Text)
    permission = db.relationship(
        "Permissions",
        secondary=RolesPermissionsTbl,
        lazy="subquery",
        backref=db.backref("roles", lazy=True),
    )
