from authentications.extensions import db


class Permissions(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False, unique=True, autoincrement=True)
    internal_api = db.Column(db.Boolean, nullable=False, index=True)
    api_name = db.Column(db.String(64), nullable=False, index=True)
    resource = db.Column(db.String(64), nullable=False)
    description = db.Column(db.Text)
    create = db.Column(db.Boolean)
    read = db.Column(db.Boolean)
    update = db.Column(db.Boolean)
    delete = db.Column(db.Boolean)
