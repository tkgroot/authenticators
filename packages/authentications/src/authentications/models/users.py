from datetime import datetime
from authentications.extensions import db, bcrypt, ma
from authentications.utils.mixins.model import TimestampMixin


class Users(TimestampMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False, autoincrement=True)
    uuid = db.Column(db.String(64), unique=True, index=True)
    username = db.Column(db.String(64), unique=True)
    password = db.Column(db.String(64))
    login_count = db.Column(db.Integer, nullable=False, default=0)
    activated = db.Column(db.Boolean, nullable=False, default=0)

    def __repr__(self):
        return f"<User {self.id}>"

    def set_password(self, password):
        self.password = bcrypt.generate_password_hash(password=password, rounds=10)

    def has_valid_password(self, password):
        return bcrypt.check_password_hash(pw_hash=self.password, password=password)

    def has_logged_in(self):
        self.login_count = self.login_count + 1
        db.session.add(self)
        db.session.commit()


class UsersSchema(ma.Schema):
    class Meta:
        fields = ("username", "email", "uuid")


user_schema = UsersSchema()
users_schema = UsersSchema(many=True)
