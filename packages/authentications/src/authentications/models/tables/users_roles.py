from authentications.extensions import db

UsersRolesTbl = db.Table(
    "users_roles",
    db.Model.metadata,
    db.Column("user_id", db.Integer, primary_key=True),
    db.Column("role_id", db.Integer, primary_key=True),
    db.ForeignKeyConstraint(["user_id", "role_id"], ["users.id", "roles.id"]),
)
