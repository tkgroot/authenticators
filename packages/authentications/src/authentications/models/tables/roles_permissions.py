from authentications.extensions import db


RolesPermissionsTbl = db.Table(
    "roles_permissions",
    db.Model.metadata,
    db.Column("roles", db.Integer, db.ForeignKey("roles.id"), primary_key=True),
    db.Column("permissions_api", db.Integer, primary_key=True),
    db.Column("permissions_resource", db.Integer, primary_key=True),
    db.ForeignKeyConstraint(
        ["permissions_api", "permissions_resource"],
        ["permissions.api_name", "permissions.resource"],
    ),
)
