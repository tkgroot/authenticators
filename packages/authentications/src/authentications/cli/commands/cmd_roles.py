import click
from flask.cli import AppGroup
from authentications.extensions import db
from authentications.models.roles import Roles
from authentications.models.permissions import Permissions
from authentications.models.tables.roles_permissions import RolesPermissionsTbl

roles_cli = AppGroup("roles")


def create(name, internal, api, resource, crud):
    c, r, u, d = crud

    db.create_all()
    role = Roles(name=name)
    permission = Permissions(
        internal_api=internal,
        api_name=(lambda: api, lambda: "localhost")[internal](),
        resource=resource,
        create=bool(c),
        read=bool(r),
        update=bool(u),
        delete=bool(d),
    )
    role.permission.append(permission)
    db.session.add(role)
    return


@roles_cli.command("create")
@click.argument("name", default="admin")
@click.option(
    "--internal",
    "-in",
    default=False,
    type=bool,
    is_flag=True,
    help="flags the api as internal",
)
@click.option(
    "--api", type=str, default="localhost", help="add api name. overwritten by --internal"
)
@click.option("-r", "--resource", type=str, default="/", help="define the resource endpoint.")
@click.option("--crud", nargs=4, type=int, default=(0, 0, 0, 0), help="add the crud rights.")
def roles_create(name, internal, api, resource, crud):
    """Add new role to the database.

    :param name: name of the role
    :type name: str
    :param internal: [description]
    :type internal: [type]
    :param resource: [description]
    :type resource: [type]
    :param crud: [description]
    :type crud: [type]
    """
    role = Roles.query.filter_by(name=name).first()

    if role:
        click.echo("Role does already exist.")
        return

    create(name, internal, api, resource, crud)

    # TODO: promting to validate input is correct
    db.session.commit()
    return


@roles_cli.command("update")
@click.argument("role")
@click.argument("permission")
@click.option("-in", "--internal")
def users_update(username, uuid, password, reset):
    """Update role and permissions."""
    return


@roles_cli.command("delete")
@click.argument("username")
@click.option("--keep", help="flag user as deactivated instead of removing him from the db.")
def users_delete(username):
    """Remove role from the database."""
    return


@roles_cli.command("ls")
def roles_list():
    """Lists all roles in the database."""
    # roles = Roles.query().all()
    roles = db.session.query(Roles).join(RolesPermissionsTbl).all()

    click.echo("role, permissions")
    for role in roles:
        click.echo(f"{role.name}, {[(p.api_name, p.resource, p.create) for p in role.permission]}")
    return
