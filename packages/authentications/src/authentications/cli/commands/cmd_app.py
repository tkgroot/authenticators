import click
from flask.cli import AppGroup
from authentications.extensions import db
from authentications.cli.commands.cmd_roles import create as roles_create
from authentications.cli.commands.cmd_users import create as users_create

app_cli = AppGroup("app")


def abort_if_false(ctx, param, value):
    if not value:
        ctx.abort()


@app_cli.command("init")
def app_init():
    """Initializes the application."""
    db.create_all()

    # add minimal admin role
    roles_create("admin", True, "localhost", "users", (1, 1, 1, 1))
    roles_create("admin", True, "localhost", "roles", (1, 1, 1, 1))
    db.session.commit()

    # add user admin with role admin
    # users_create("admin", "admin", "admin")
    return


@app_cli.command("drop")
@click.option("-f", is_flag=True, callback=abort_if_false, expose_value=False, prompt="drop db?")
def app_drop():
    """Drop database."""
    db.drop_all()
    return
