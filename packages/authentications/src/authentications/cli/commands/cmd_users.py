import click
from datetime import datetime
from uuid import uuid4
from flask.cli import AppGroup
from bcrypt import hashpw, gensalt
from authentications.models.users import Users
from authentications.extensions import db

users_cli = AppGroup("users")


def create():
    pass


@users_cli.command("create")
@click.argument("username")
@click.argument("password")
@click.option("--admin", is_flag=True, help="give user full admin permissions.")
@click.option("--active", is_flag=True, help="activate user account. With --admin its set to true.")
def users_create(username, password, admin, active):
    """Add user to the database."""
    user = Users(
        uuid=str(uuid4()),
        username=username,
        password=hashpw(f"{password}".encode(), gensalt(10)),
        activated=(False, True)[admin or active],
        created_at=datetime.utcnow(),
    )

    db.create_all()
    db.session.add(user)
    db.session.commit()

    click.echo(f"id, username, uuid\n {user.id}, {user.username}, {user.uuid}")
    return


@users_cli.command("delete")
@click.argument("username")
@click.option(
    "--keep", is_flag=True, help="flag user as deactivated instead of removing him from the db."
)
def users_delete(username, keep):
    """Remove user from the database."""
    user = Users.query.filter_by(username=username).first()

    if user is None:
        click.echo(f"username {username} not found.")
        return

    if keep:
        user = Users.query.filter_by(username=username).first()
        user.activated = False
        db.session.add(user)
    else:
        db.session.delete(user)

    db.session.commit()
    click.echo(f"user {username} deleted.")
    return


@users_cli.command("update")
@click.argument("username")
@click.option("--uuid", default=False, type=bool, is_flag=True, help="updates the UUID field.")
@click.option("--password", default=None, help="updates password ")
@click.option(
    "--reset", default=False, type=bool, is_flag=True, help="reset account to be prestine"
)
def users_update(username, uuid, password, reset):
    """Update user fields in the database."""
    user = Users.query.filter_by(username=username).first()

    if user is None:
        click.echo(f"user with username={username} not found")
        return

    if password is not None:
        user.password = hashpw(f"{password}".encode(), gensalt(10))
        user.updated_at = datetime.utcnow()
        click.echo("changed password.")

    if uuid:
        uuid_old = user.uuid
        user.uuid = str(uuid4())
        user.update_at = datetime.utcnow()
        click.echo(f"changed {uuid_old} => {user.uuid}")

    if reset:
        user.created_at = datetime.utcnow()
        user.login_count = 0
        user.updated_at = None
        click.echo("user resetted.")

    db.session.add(user)
    db.session.commit()
    return


@users_cli.command("ls")
def users_list():
    """Lists all users in the database."""
    users = Users.query.all()

    click.echo("username, uuid")
    for user in users:
        click.echo(f"{user.username}, {user.uuid}")

    return
