from flask import Flask
from flask_restful import Api
from authentications.config import settings
from authentications.extensions import jwt, bcrypt, db, migrate, ma
from authentications.resources.users.auth import Authentication
from authentications.resources.protected import Protected
from authentications.cli.commands.cmd_users import users_cli
from authentications.cli.commands.cmd_roles import roles_cli
from authentications.cli.commands.cmd_app import app_cli


def create_app():
    app = Flask(__name__, instance_relative_config=True)
    api = Api(app)

    app.config.from_object(settings)
    app.config.from_pyfile("settings.py", silent=True)

    app.cli.add_command(users_cli)
    app.cli.add_command(roles_cli)
    app.cli.add_command(app_cli)
    extensions(app)

    api_prefix = app.config["VERSION_PREFIX"]
    api.add_resource(Authentication, f"/api/{api_prefix}/users/auth")
    api.add_resource(Protected, f"/api/{api_prefix}/protected")

    @app.route("/")
    def index():
        return "Hello World"

    return app


def extensions(app):
    jwt.init_app(app)
    bcrypt.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)
    ma.init_app(app)
