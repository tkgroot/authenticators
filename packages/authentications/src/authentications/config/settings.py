"""Application Configurations."""
import os
from dotenv import load_dotenv

load_dotenv()

VERSION_PREFIX = "v1"
DEBUG = 1

# Flask-JWT-{SIMPLE,EXTENDED} Settings
JWT_SECRET_KEY = os.getenv("JWT_SECRET_KEY", "secret")

# ORM Settings
SQLALCHEMY_DATABASE_URI = os.getenv("SQLALCHEMY_DATABASE_URI", f"sqlite:///{os.path.join(os.getcwd(), 'sqlite.db')}")
SQLALCHEMY_TRACK_MODIFICATIONS = 0
